from django.shortcuts import render
from django.views.generic import ListView,DetailView,CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.urls import reverse_lazy
from .forms import BlogForm,ImageUploadForm
from .models import Blog
from django.contrib import messages
import calendar
from collections import deque
import datetime
from django.views import generic
from django.http import HttpResponseRedirect
import numpy as np
from PIL import Image
from io import BytesIO
import base64
from .lib import predict,build_model
from keras.models import Sequential,load_model
import tensorflow as tf

graph = tf.get_default_graph()

class AnimalView(generic.FormView):
    template_name = 'blog/blog_animal.html'
    form_class = ImageUploadForm


    def form_valid(self, form):
        global graph
        with graph.as_default():
            classes = ["monkey", "boar", "crow", "men", "women"]
            num_classes = len(classes)  # 格納された画像の数を数える。
            image_size = 50  # 画像サイズを小さくする。

            file = form.cleaned_data['file']
            image = Image.open(file)  # なんかうまくいかないので、ここに直接打ち込んだ
            image = image.convert('RGB')
            image = image.resize((image_size, image_size))
            data = np.asarray(image)  # numpyの配列に変換
            X = []
            X.append(data)
            X = np.array(X)
            model = build_model()
            result = model.predict([X])[0]  # 推定結果を得る 予想結果が配列になっている。
            predicted = result.argmax()  # 推定値の中で、最も推定値の大きいものを得る
            percentage = int(result[predicted] * 100)

            # 推論した結果を、テンプレートへ渡して表示
            context = {
                'result': classes[predicted],
                'percentage':percentage
            }

            return render(self.request, 'blog/blog_animal_result.html', context)

class LineView(generic.TemplateView):
    model = Blog
    template_name = 'blog/blog_line.html'  
class PaintView(generic.TemplateView):
    template_name = 'blog/blog_paint.html'

    def post(self, request):
        base_64_string = request.POST['img-src'].replace(
            'data:image/png;base64,', '')
        file = BytesIO(base64.b64decode(base_64_string))

        # ファイルを、28*28にリサイズし、グレースケール(モノクロ画像)
        img = Image.open(file).resize((28, 28)).convert('L')

        # 学習時と同じ形に画像データを変換する
        img_array = np.asarray(img) / 255
        img_array = img_array.reshape(1, 784)

        # 推論した結果を、テンプレートへ渡して表示
        context = {
            'result': predict(img_array),
        }
        return render(self.request, 'blog/blog_result.html', context)

class UploadView(generic.FormView):
    template_name = 'blog/blog_upload.html'
    form_class = ImageUploadForm

    def form_valid(self, form):
        # アップロードファイル本体を取得
        file = form.cleaned_data['file']

        # ファイルを、28*28にリサイズし、グレースケール(モノクロ画像)
        img = Image.open(file).resize((50, 50)).convert('L')

        # 学習時と同じ形に画像データを変換する
        img_array = np.asarray(img) / 255
        img_array = img_array.reshape(1, 784)

        # 推論した結果を、テンプレートへ渡して表示
        context = {
            'result': predict(img_array),
        }
        return render(self.request, 'blog/blog_result.html', context)

class BaseCalendarMixin:
    """カレンダー関連Mixinの、基底クラス"""
    first_weekday = 6  # 0は月曜から、1は火曜から。6なら日曜日からになります。お望みなら、継承したビューで指定してください。
    week_names = ['mon', 'tue', 'wed', 'thu', 'fri', 'str', 'sun']  # これは、月曜日から書くことを想定します。['Mon', 'Tue'...

    def setup(self):
        """カレンダーのセットアップ処理

        calendar.Calendarクラスの機能を利用するため、インスタンス化します。
        Calendarクラスのmonthdatescalendarメソッドを利用していますが、デフォルトが月曜日からで、
        火曜日から表示したい(first_weekday=1)、といったケースに対応するためのセットアップ処理です。

        """
        self._calendar = calendar.Calendar(self.first_weekday)

class MonthCalendarMixin(BaseCalendarMixin):
    """月間カレンダーの機能を提供するMixin"""

    @staticmethod
    def get_previous_month(date):
        """前月を返す"""
        if date.month == 1:
            return date.replace(year=date.year-1, month=12, day=1)

        else:
            return date.replace(month=date.month-1, day=1)

    @staticmethod
    def get_next_month(date):
        """次月を返す"""
        if date.month == 12:
            return date.replace(year=date.year+1, month=1, day=1)

        else:
            return date.replace(month=date.month+1, day=1)

    def get_month_days(self, date):
        """その月の全ての日を返す"""
        return self._calendar.monthdatescalendar(date.year, date.month)

    def get_current_month(self):
        """現在の月を返す"""
        month = self.kwargs.get('month')
        year = self.kwargs.get('year')
        if month and year:
            month = datetime.date(year=int(year), month=int(month), day=1)
        else:
            month = datetime.date.today().replace(day=1)
        return month

    def get_month_calendar(self):
        """月間カレンダー情報の入った辞書を返す"""
        self.setup()
        current_month = self.get_current_month()
        calendar_data = {
            'now': datetime.date.today(),
            'days': self.get_month_days(current_month),
            'current': current_month,
            'previous': self.get_previous_month(current_month),
            'next': self.get_next_month(current_month),
            'week_names': self.get_week_names(),
        }
        return calendar_data


    def get_week_names(self):
        """first_weekday(最初に表示される曜日)にあわせて、week_namesをシフトする"""
        week_names = deque(self.week_names)
        week_names.rotate(-self.first_weekday)  # リスト内の要素を右に1つずつ移動...なんてときは、dequeを使うと中々面白いです
        return week_names

class MonthCalendar(MonthCalendarMixin, generic.TemplateView):
    """月間カレンダーを表示するビュー"""
    template_name = 'blog/blog_month.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['month'] = self.get_month_calendar()
        return context

class BlogDeleteView(DeleteView):
    model = Blog
    success_url = reverse_lazy("index")

    def delete(self, request, *args, **kwargs):
        messages.success(self.request,"削除しました")
        return super().delete(request,*args, **kwargs)


class BlogListView(ListView):
    model = Blog
    paginate_by = 5 #一度に読み込むページ数
    context_object_name =  "blogs" #読み込ための変数名を変える。元々はobject_list

class BlogDetailView(DetailView):
    model = Blog
    context_object_name = "blog"  # 読み込ための変数名を変える。元々はobject　両方使えるようになる。

class BlogCreateView(CreateView):
    model = Blog
    form_class = BlogForm
    success_url = reverse_lazy("index")
    template_name = "blog/blog_create_form.html"

    def form_valid(self, form):
        messages.success(self.request, "保存しました")
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "保存に失敗しました")
        return super().form_invalid(form)

class BlogUpdateView(UpdateView):
    model = Blog
    form_class = BlogForm
    template_name = "blog/blog_update_form.html"

    def get_success_url(self):
        return reverse_lazy("detail", kwargs={"pk": self.kwargs["pk"]})

    def form_valid(self, form):
        messages.success(self.request, "保存しました")
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "保存に失敗しました")
        return super().form_invalid(form)

# Create your views here.

from django import forms
from .models import Blog

class BlogForm(forms.ModelForm):

    content = forms.CharField(widget=forms.TextInput(attrs={"size": 100}))

    class Meta:
        model = Blog
        fields = ["content", ]


class ImageUploadForm(forms.Form):
    file = forms.ImageField(label='画像ファイル')
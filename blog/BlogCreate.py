from django.views.generic import CreateView
from django.urls import reverse_lazy
from .models import Blog


class BlogCreateView(CreateView):
    model = Blog
    fields = ["content", ]
    success_url = reverse_lazy("index")

inputl = input()
inputl = inputl.split()
taxnum, kyori, total = int(inputl[0]), int(inputl[1].strip()), []

for i in range(taxnum):
    total.append(0)

for i in range(taxnum):
    inputl = input()
    inputl = inputl.split()
    hatu_kyori, hatu_money, kasan_kyori, kasan_money = int(inputl[0]), int(inputl[1].strip()), int(
        inputl[2].strip()), int(inputl[3].strip())

    if hatu_kyori > kyori:
        total[i] = hatu_money
    else:
        o, num, nokori = 1, 0, kyori - hatu_kyori
        while num < 10:
            total[i] = hatu_money + kasan_money * o
            if nokori < kasan_kyori * o:
                break
            o += 1

print(min(total), max(total))

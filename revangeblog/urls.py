"""revangeblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from blog.views import BlogListView
from blog.views import BlogDetailView
from blog.views import BlogCreateView
from blog.views import BlogUpdateView
from blog.views import BlogDeleteView
from blog.views import MonthCalendar
from blog.views import PaintView
from blog.views import UploadView,AnimalView,LineView


app_name = 'blog'

urlpatterns = [
    path('month', MonthCalendar.as_view(), name='month'),
    path('month/<int:year>/<int:month>/', MonthCalendar.as_view(), name='month'),
    path('',BlogListView.as_view(), name="index"),
    path('<int:pk>/delete', BlogDeleteView.as_view(), name="delete"),
    path('create',BlogCreateView.as_view(), name="create"),
    path('<int:pk>', BlogDetailView.as_view(), name="detail"),
    path('admin/', admin.site.urls),
    path('<int:pk>/update',BlogUpdateView.as_view(),name="update"),
    path('predict', UploadView.as_view(), name='upload'),
    path('paint', PaintView.as_view(), name='paint'),
    path('line', LineView.as_view(), name='line'),
    path('animal', AnimalView.as_view(), name='animal')
]

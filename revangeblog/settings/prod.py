from .common import *

DEBUG = False
ALLOWED_HOSTS = ['*','192.168.100.116']
INSTALLED_APPS += (
    'gunicorn',
)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'revangeblog',
        'USER': 'revangeblog',
        'PASSWORD': 'revangeblog',

    }
}

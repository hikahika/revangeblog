from fabric.api import cd, sudo, shell_env, env

HOST_PATH = "/ opt / django / revangeblog2 / revangeblog"

ex_env = {"DJANGO_SETTINGS_MODULE": "revangeblog.settings.prod",}

env.hosts = ["192.168.100.116",]
env.user = "root"
env.password = "hikahika7"


def collect_static():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            sudo(" / opt / django / revangeblog2 / bin / python manage.py collectstatic --no-input")

def git_update():
    with cd(HOST_PATH):
        run("git pull")

def restart():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            sudo("pkill - f gunicorn")
            sudo("/ opt / django / revangeblog2 / bin / gunicorn - c gunicorn.conf.py revangeblog.wsgi")